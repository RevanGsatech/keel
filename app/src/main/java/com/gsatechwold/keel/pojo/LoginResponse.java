package com.gsatechwold.keel.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginResponse {


    @SerializedName("status")
    @Expose
    public String status;

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public UserDetails getUserDetails() {
        return userDetails;
    }

    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("user_details")
    @Expose
    public UserDetails userDetails;


    public class UserDetails {

        @SerializedName("id")
        @Expose
        public Integer id;
        @SerializedName("name")
        @Expose
        public String name;

        public Integer getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public String getMobileNumber() {
            return mobileNumber;
        }

        public String getEmail() {
            return email;
        }

        public String getAddress() {
            return address;
        }

        @SerializedName("mobile_number")
        @Expose
        public String mobileNumber;
        @SerializedName("email")
        @Expose
        public String email;
        @SerializedName("address")
        @Expose
        public String address;

    }
}
