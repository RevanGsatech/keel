package com.gsatechwold.keel.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class JobListRespone {
    public String getStatus() {
        return status;
    }

    public List<Datum> getData() {
        return data;
    }

    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("data")
    @Expose
    public List<Datum> data = null;


    public class Datum {

        @SerializedName("id")
        @Expose
        public Integer id;

        public Integer getId() {
            return id;
        }

        public String getJobTitle() {
            return jobTitle;
        }

        public String getJobDescription() {
            return jobDescription;
        }

        public String getJobAddress() {
            return jobAddress;
        }

        public String getVillage() {
            return village;
        }

        public String getContact() {
            return contact;
        }

        public Integer getAppliedCount() {
            return appliedCount;
        }

        public String getDateTime() {
            return dateTime;
        }

        public String getImage() {
            return image;
        }

        public String getPricing() {
            return pricing;
        }

        public String getDistance() {
            return distance;
        }

        @SerializedName("job_title")
        @Expose
        public String jobTitle;
        @SerializedName("job_description")
        @Expose
        public String jobDescription;
        @SerializedName("job_address")
        @Expose
        public String jobAddress;
        @SerializedName("village")
        @Expose
        public String village;
        @SerializedName("contact")
        @Expose
        public String contact;
        @SerializedName("applied_count")
        @Expose
        public Integer appliedCount;
        @SerializedName("date_time")
        @Expose
        public String dateTime;
        @SerializedName("image")
        @Expose
        public String image;
        @SerializedName("pricing")
        @Expose
        public String pricing;
        @SerializedName("distance")
        @Expose
        public String distance;

    }
}
