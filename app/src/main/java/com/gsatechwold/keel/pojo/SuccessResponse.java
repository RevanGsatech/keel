package com.gsatechwold.keel.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SuccessResponse {

    @SerializedName("status")
    @Expose
    public String response;

    public String getResponse() {
        return response;
    }

    public String getMessage() {
        return message;
    }

    @SerializedName("message")
    @Expose
    public String message;
}
