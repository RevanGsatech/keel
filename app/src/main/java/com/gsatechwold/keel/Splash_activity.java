package com.gsatechwold.keel;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.gsatechwold.keel.databinding.SplashScreenBinding;
import com.gsatechwold.keel.ui.DashBoardActivity;
import com.gsatechwold.keel.ui.base.BaseActivity;


public class Splash_activity extends BaseActivity {

    SplashScreenBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.splash_screen);

        int secondsDelayed = 1;

        new Handler().postDelayed(() -> {

            if (sessionManager.isUserLoggedIn()) {

                Intent intent = new Intent(Splash_activity.this, DashBoardActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();

            } else {
                Intent intent = new Intent(Splash_activity.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();

            }

        }, secondsDelayed * 2000);

    }
}

