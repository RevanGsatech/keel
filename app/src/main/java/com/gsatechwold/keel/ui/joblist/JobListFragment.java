package com.gsatechwold.keel.ui.joblist;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.gsatechwold.keel.R;
import com.gsatechwold.keel.databinding.JoblistfragmentBinding;
import com.gsatechwold.keel.pojo.JobListRespone;
import com.gsatechwold.keel.ui.adapter.AdapterJobList;
import com.gsatechwold.keel.ui.base.BaseFragment;
import com.gsatechwold.keel.ui.utils.CommonUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class JobListFragment extends BaseFragment {


    private JoblistfragmentBinding binding;
    private List<JobListRespone.Datum> ordersArrayList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.joblistfragment, container, false);

        timeLineAPI();

        return binding.getRoot();

    }


    private void timeLineAPI() {

        showProgressDialog();

        if (CommonUtils.isConnectingToInternet(getContext())) {
            Call<JobListRespone> call = networkAPI.timeline(sessionManager.getUserID(), "0.0", "0.0");
            call.enqueue(new Callback<JobListRespone>() {
                @SuppressLint("Assert")
                @Override
                public void onResponse(@NonNull Call<JobListRespone> call, @NonNull Response<JobListRespone> response) {
                    JobListRespone serverResponse = response.body();

                    dismissProgressDialog();

                    if (response.body() != null) {

                        assert serverResponse != null;

                        if (serverResponse.getStatus().equals("success")) {

                            ordersArrayList = serverResponse.getData();
                            setOrderAdapter();
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<JobListRespone> call, @NonNull Throwable t) {
                    Log.v("Response", Objects.requireNonNull(t.getMessage()));
                    dismissProgressDialog();
                }
            });
        } else {
            CommonUtils.showerrormsg("Error", "No Internet Connection", getContext());
        }

    }

    private void setOrderAdapter() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
        binding.recycleView.setLayoutManager(layoutManager);
        AdapterJobList adapterJobList = new AdapterJobList(ordersArrayList);
        binding.recycleView.setAdapter(adapterJobList);

        adapterJobList.setClickListener((view, position) -> {
            Intent intent = new Intent(getActivity(), JobDetailActivity.class);
            intent.putExtra("array", new Gson().toJson(ordersArrayList.get(position)));
            startActivity(intent);

        });

    }

}
