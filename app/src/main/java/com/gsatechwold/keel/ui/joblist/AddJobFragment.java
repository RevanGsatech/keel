package com.gsatechwold.keel.ui.joblist;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.gsatechwold.keel.R;
import com.gsatechwold.keel.array.InputAddJob;
import com.gsatechwold.keel.databinding.AddemployeeBinding;
import com.gsatechwold.keel.pojo.SuccessResponse;
import com.gsatechwold.keel.ui.DashBoardActivity;
import com.gsatechwold.keel.ui.base.BaseFragment;
import com.gsatechwold.keel.ui.utils.CommonUtils;

import java.util.Objects;

import in.mayanknagwanshi.imagepicker.ImageSelectActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.gsatechwold.keel.ui.utils.CommonUtils.basecode_convert;

public class AddJobFragment extends BaseFragment implements View.OnTouchListener, View.OnClickListener {

    AddemployeeBinding binding;
    String image = "";
    InputAddJob inputAddJob;

    @SuppressLint("ClickableViewAccessibility")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.addemployee, container, false);

        binding.jobTitle.setOnTouchListener(this);
        binding.jobDesc.setOnTouchListener(this);
        binding.village.setOnTouchListener(this);
        binding.jobAddress.setOnTouchListener(this);
        binding.jobSalary.setOnTouchListener(this);
        binding.jobDays.setOnTouchListener(this);
        binding.jobTotal.setOnTouchListener(this);
        binding.jobContact.setOnTouchListener(this);
        binding.jobImage.setOnClickListener(this);
        binding.Submit.setOnClickListener(this);

        return binding.getRoot();
    }

    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.job_image) {

            Intent intent1 = new Intent(getContext(), ImageSelectActivity.class);
            intent1.putExtra(ImageSelectActivity.FLAG_CAMERA, true);//default is true
            intent1.putExtra(ImageSelectActivity.FLAG_GALLERY, true);//default is true
            startActivityForResult(intent1, 1242);

        } else if (view.getId() == R.id.Submit) {

            if (isFieldsAreValid()) {

                addjobApi();

            }
        }
    }

    private boolean isFieldsAreValid() {
        String jobTitle = Objects.requireNonNull(binding.jobTitle.getText()).toString();
        String jobDesc = Objects.requireNonNull(binding.jobDesc.getText()).toString();
        String village = Objects.requireNonNull(binding.village.getText()).toString();
        String jobAddress = Objects.requireNonNull(binding.jobAddress.getText()).toString();
        String jobSalary = Objects.requireNonNull(binding.jobSalary.getText()).toString();
        String jobDays = Objects.requireNonNull(binding.jobDays.getText()).toString();
        String jobTotal = Objects.requireNonNull(binding.jobTotal.getText()).toString();
        String jobContact = Objects.requireNonNull(binding.jobContact.getText()).toString();


        if (TextUtils.isEmpty(jobTitle)) {
            binding.jobTitle.requestFocus();
            binding.jobTitle.setError("Enter Job Title ");
            return false;
        } else if (TextUtils.isEmpty(jobDesc)) {
            binding.jobDesc.requestFocus();
            binding.jobDesc.setError("Enter JOb Description");
            return false;
        } else if (TextUtils.isEmpty(village)) {
            binding.village.requestFocus();
            binding.village.setError("Enter village");
            return false;
        } else if (TextUtils.isEmpty(jobAddress)) {
            binding.jobAddress.requestFocus();
            binding.jobAddress.setError("Enter Address");
            return false;
        } else if (TextUtils.isEmpty(jobSalary)) {
            binding.jobSalary.requestFocus();
            binding.jobSalary.setError("Enter Salary amount");
            return false;
        } else if (TextUtils.isEmpty(jobDays)) {
            binding.jobDays.requestFocus();
            binding.jobDays.setError("Enter No days");
            return false;
        } else if (TextUtils.isEmpty(jobTotal)) {
            binding.jobTotal.requestFocus();
            binding.jobTotal.setError("Enter No of Jobs ");
            return false;
        } else if (TextUtils.isEmpty(jobContact)) {
            binding.jobContact.requestFocus();
            binding.jobContact.setError("Enter Contact no.");
            return false;
        }

        inputAddJob = new InputAddJob(sessionManager.getUserID(), image, jobTitle, jobDesc, jobAddress, village, "0.0", "0.0",
                jobSalary, jobDays, jobTotal, jobContact);

        return true;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouch(View view, MotionEvent event) {
        if (view instanceof EditText) {
            view.setFocusableInTouchMode(true);
            ((EditText) view).setCursorVisible(true);
        }
        return false;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1242 && resultCode == Activity.RESULT_OK) {
            String filePath = data.getStringExtra(ImageSelectActivity.RESULT_FILE_PATH);
            Bitmap selectedImage = BitmapFactory.decodeFile(filePath);

            binding.jobImage.setImageBitmap(selectedImage);
            image = basecode_convert(filePath);

        }
    }

    private void addjobApi() {

        showProgressDialog();

        if (CommonUtils.isConnectingToInternet(getContext())) {
            Call<SuccessResponse> call = networkAPI.addEmployerForm(inputAddJob);
            call.enqueue(new Callback<SuccessResponse>() {
                @SuppressLint("Assert")
                @Override
                public void onResponse(@NonNull Call<SuccessResponse> call, @NonNull Response<SuccessResponse> response) {
                    SuccessResponse serverResponse = response.body();

                    dismissProgressDialog();

                    if (response.body() != null) {

                        assert serverResponse != null;

                        if (serverResponse.getResponse().equals("success")) {


                            Toast.makeText(getContext(), serverResponse.getMessage(), Toast.LENGTH_SHORT).show();

                            Intent intent = new Intent(getContext(), DashBoardActivity.class);
                            startActivity(intent);


                        } else {

                            CommonUtils.showerrormsg("Success", serverResponse.getMessage(), getActivity());

                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<SuccessResponse> call, @NonNull Throwable t) {
                    Log.v("Response", Objects.requireNonNull(t.getMessage()));
                    dismissProgressDialog();
                }
            });
        } else {
            CommonUtils.showerrormsg("Error", "No Internet Connection", getContext());
        }

    }

}
