package com.gsatechwold.keel.ui.login;


import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.lang.reflect.Array;

public class LoginViewModel extends ViewModel {

    private MutableLiveData<Array> users;
    public LiveData<Array> getUsers() {
        if (users == null) {
            users = new MutableLiveData<Array>();
            loadUsers();
        }
        return users;
    }

    private void loadUsers() {
        // Do an asynchronous operation to fetch users.

    }
}
