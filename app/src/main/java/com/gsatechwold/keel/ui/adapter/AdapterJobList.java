package com.gsatechwold.keel.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.gsatechwold.keel.R;
import com.gsatechwold.keel.databinding.AdapterjobtimelineBinding;
import com.gsatechwold.keel.pojo.JobListRespone;

import java.util.List;


public class AdapterJobList extends RecyclerView.Adapter<AdapterJobList.ViewHolder> {

    private Context context;
    private List<JobListRespone.Datum> ordersArrayList;
    private ItemClickListener clickListener;

    public AdapterJobList(List<JobListRespone.Datum> ordersArrayList) {

        this.ordersArrayList = ordersArrayList;


    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        AdapterjobtimelineBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.adapterjobtimeline, parent, false);

        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.adpterquestionlistBinding.setData(ordersArrayList.get(position));

    }

    @Override
    public int getItemCount() {
        return ordersArrayList.size();
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }


    public interface ItemClickListener {
        void onClick(View view, int position);
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        AdapterjobtimelineBinding adpterquestionlistBinding;

        ViewHolder(AdapterjobtimelineBinding adapterhistoryBinding) {
            super(adapterhistoryBinding.getRoot());
            adpterquestionlistBinding = adapterhistoryBinding;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (clickListener != null) clickListener.onClick(view, getPosition());
        }
    }

}