package com.gsatechwold.keel.ui.joblist;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.google.gson.Gson;
import com.gsatechwold.keel.R;
import com.gsatechwold.keel.array.InputApplyJob;
import com.gsatechwold.keel.databinding.JobdetaillayoutBinding;
import com.gsatechwold.keel.pojo.JobListRespone;
import com.gsatechwold.keel.pojo.SuccessResponse;
import com.gsatechwold.keel.ui.base.BaseActivity;
import com.gsatechwold.keel.ui.utils.CommonUtils;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class JobDetailActivity extends BaseActivity {

    JobListRespone.Datum data;

    JobdetaillayoutBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.jobdetaillayout);
        setToolBar(binding.included.toolbar, "Job Detail");

        Intent intent = getIntent();
        if (intent != null) {
            String dd = intent.getStringExtra("array");
            data = new Gson().fromJson(dd, JobListRespone.Datum.class);
        }

        binding.setData(data);

        binding.btSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addjobApi();
            }
        });


    }

    private void addjobApi() {

        showProgressDialog();

        if (CommonUtils.isConnectingToInternet(this)) {
            Call<SuccessResponse> call = networkAPI.applyJob(new InputApplyJob(sessionManager.getUserID(), String.valueOf(data.getId())));
            call.enqueue(new Callback<SuccessResponse>() {
                @SuppressLint("Assert")
                @Override
                public void onResponse(@NonNull Call<SuccessResponse> call, @NonNull Response<SuccessResponse> response) {
                    SuccessResponse serverResponse = response.body();

                    dismissProgressDialog();

                    if (response.body() != null) {

                        assert serverResponse != null;

                        if (serverResponse.getResponse().equals("success")) {

                            CommonUtils.showerrormsg("Success", serverResponse.getMessage(), JobDetailActivity.this);
                            binding.btSubmit.setText("Applied");

                        } else {

                            CommonUtils.showerrormsg("fail", serverResponse.getMessage(), JobDetailActivity.this);

                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<SuccessResponse> call, @NonNull Throwable t) {
                    Log.v("Response", Objects.requireNonNull(t.getMessage()));
                    dismissProgressDialog();
                }
            });
        } else {
            CommonUtils.showerrormsg("Error", "No Internet Connection", this);
        }

    }
}
