package com.gsatechwold.keel.ui.reg;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.gsatechwold.keel.R;
import com.gsatechwold.keel.array.InputReg;
import com.gsatechwold.keel.databinding.ActivityregBinding;
import com.gsatechwold.keel.pojo.SuccessResponse;
import com.gsatechwold.keel.ui.base.BaseActivity;
import com.gsatechwold.keel.ui.login.LoginViewModel;
import com.gsatechwold.keel.ui.utils.CommonUtils;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.gsatechwold.keel.ui.utils.CommonUtils.setFocus;


public class RegistrationActivity extends BaseActivity implements View.OnTouchListener, View.OnClickListener {

    ActivityregBinding binding;
    LoginViewModel mViewModel;
    InputReg inputReg;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);
        binding = DataBindingUtil.setContentView(this, R.layout.activityreg);

        setToolBar(binding.included.toolbar, "Registration");

        // OnTouch Click Listener
        binding.name.setOnTouchListener(this);
        binding.mobileNo.setOnTouchListener(this);
        binding.emailId.setOnTouchListener(this);
        binding.village.setOnTouchListener(this);
        binding.address.setOnTouchListener(this);
        binding.education.setOnTouchListener(this);
        binding.etPassword.setOnTouchListener(this);
        binding.confEtPassword.setOnTouchListener(this);

        // OnClick Listener
        binding.Submit.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (isFieldsAreValid()) {
            registrationAPi();
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouch(View view, MotionEvent event) {
        if (view instanceof EditText) {
            view.setFocusableInTouchMode(true);
            ((EditText) view).setCursorVisible(true);
        }
        return false;
    }

    private boolean isFieldsAreValid() {

        String name = getString(binding.name);
        String mobileNumber = getString(binding.mobileNo);
        String etEmail = getString(binding.emailId);
        String villageName = getString(binding.village);
        String address = getString(binding.address);
        String educations = getString(binding.education);
        String password = getString(binding.etPassword);
        String confEtPassword = getString(binding.confEtPassword);

        if (TextUtils.isEmpty(name)) {
            setFocus(binding.name, "Enter Name");
            return false;
        } else if (CommonUtils.validatePhoneNumber(mobileNumber) != null) {
            setFocus(binding.mobileNo, CommonUtils.validatePhoneNumber(mobileNumber));
            return false;
        }/* else if (CommonUtils.validateEmail(etEmail) != null) {
            setFocus(binding.emailId, CommonUtils.validateEmail(etEmail));
            return false;
        } */ else if (TextUtils.isEmpty(villageName)) {
            setFocus(binding.village, "Enter Village Name");
            return false;
        } else if (TextUtils.isEmpty(address)) {
            setFocus(binding.address, "Enter Address");
            return false;
        } else if (CommonUtils.validatePassword(password) != null) {
            setFocus(binding.etPassword, CommonUtils.validatePassword(password));
            return false;
        } else if (!password.equals(confEtPassword)) {
            setFocus(binding.confEtPassword, "Confirm Pass Not Matched");
            return false;
        }

        inputReg = new InputReg(name, mobileNumber, etEmail, villageName, address, educations, password);
        return true;
    }


    private void registrationAPi() {

        showProgressDialog();

        if (CommonUtils.isConnectingToInternet(this)) {
            Call<SuccessResponse> call = networkAPI.regisration(inputReg);
            call.enqueue(new Callback<SuccessResponse>() {
                @SuppressLint("Assert")
                @Override
                public void onResponse(@NonNull Call<SuccessResponse> call, @NonNull Response<SuccessResponse> response) {
                    SuccessResponse serverResponse = response.body();

                    dismissProgressDialog();

                    if (response.body() != null) {

                        assert serverResponse != null;
                        if (serverResponse.getResponse().equals("success")) {

                            String mobileNumber = getString(binding.mobileNo);

                            Intent intent = new Intent(RegistrationActivity.this, ActivityOtpScreen.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.putExtra("from", "reg");
                            intent.putExtra("phone", mobileNumber);
                            startActivity(intent);


                        } else {
                            fail_dialog(serverResponse.getMessage());
                        }

                    }
                }

                @Override
                public void onFailure(@NonNull Call<SuccessResponse> call, @NonNull Throwable t) {
                    Log.v("Response", Objects.requireNonNull(t.getMessage()));
                    dismissProgressDialog();
                }
            });
        } else {
            CommonUtils.showerrormsg("Error", "No Internet Connection", this);
        }

    }

    void fail_dialog(String str) {

        MaterialStyledDialog.Builder dialogHeader_1 = new MaterialStyledDialog.Builder(this)
                .setIcon(R.drawable.sad)
                .withDialogAnimation(true)
                .setTitle("Sorry!")
                .setDescription(str)
                .setHeaderColor(R.color.colorPrimaryDark)
                .setPositiveText("OK")
                .onPositive((dialog, which) -> dialog.dismiss());


        dialogHeader_1.show();
    }
}
