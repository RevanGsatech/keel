package com.gsatechwold.keel.ui.reg;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.gsatechwold.keel.R;
import com.gsatechwold.keel.array.InputVerifyOtp;
import com.gsatechwold.keel.databinding.ActivityotpBinding;
import com.gsatechwold.keel.pojo.SuccessResponse;
import com.gsatechwold.keel.ui.base.BaseActivity;
import com.gsatechwold.keel.ui.login.LoginActivity;
import com.gsatechwold.keel.ui.utils.CommonUtils;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressLint("CheckResult")
public class ActivityOtpScreen extends BaseActivity implements View.OnClickListener {
    ActivityotpBinding binding;
    String mobileNO, from;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activityotp);
        setToolBar(binding.included.toolbar, getResources().getString(R.string.otp_screen));

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mobileNO = bundle.getString("phone");
            from = bundle.getString("from");
            setStringToText(binding.tvMobileNumber, mobileNO);
        }

        binding.btnVerifyOtp.setOnClickListener(this);
        binding.btnResendOtp.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        String value = binding.pinview.getValue();
        if (view.getId() == R.id.btn_verify_otp) {

            if (isFieldsAreValid() && from.equals("forgot")) {

                Intent homeIntent = new Intent(this, ActivityResetPassword.class);
                homeIntent.putExtra("mobile", mobileNO);
                homeIntent.putExtra("otp", value);
                startActivity(homeIntent);

            } else if (isFieldsAreValid() && from.equals("reg")) {
                verifyOtp(new InputVerifyOtp(mobileNO, value));
            }

        } else if (view.getId() == R.id.btn_resend_otp) {

            resentOtp();

        }
    }

    private boolean isFieldsAreValid() {
        String value = binding.pinview.getValue();
        if (TextUtils.isEmpty(value) || value.length() < 4) {
            Toast.makeText(this, "Enter valid OTP", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void verifyOtp(InputVerifyOtp inputVerifyOtp) {

        showProgressDialog();

        if (CommonUtils.isConnectingToInternet(this)) {
            Call<SuccessResponse> call = networkAPI.verifyotp(inputVerifyOtp);
            call.enqueue(new Callback<SuccessResponse>() {
                @SuppressLint("Assert")
                @Override
                public void onResponse(@NonNull Call<SuccessResponse> call, @NonNull Response<SuccessResponse> response) {
                    SuccessResponse serverResponse = response.body();

                    dismissProgressDialog();

                    if (response.body() != null) {

                        assert serverResponse != null;
                        if (serverResponse.getResponse().equals("success")) {

                            callIntent(serverResponse.getMessage());
                        } else {
                            CommonUtils.showerrormsg("Error", serverResponse.getMessage(), ActivityOtpScreen.this);
                        }

                    }
                }

                @Override
                public void onFailure(@NonNull Call<SuccessResponse> call, @NonNull Throwable t) {
                    Log.v("Response", Objects.requireNonNull(t.getMessage()));
                    dismissProgressDialog();
                }
            });
        } else {
            CommonUtils.showerrormsg("Error", "No Internet Connection", this);
        }

    }


    void callIntent(String msg) {

        MaterialStyledDialog.Builder dialogHeader_1 = new MaterialStyledDialog.Builder(this)
                .setIcon(R.drawable.smiley)
                .withDialogAnimation(true)
                .setTitle("Success!")
                .setDescription(msg)
                .setHeaderColor(R.color.colorPrimaryDark)
                .setPositiveText("OK")
                .onPositive((dialog, which) -> {
                            Intent homeIntent = new Intent(this, LoginActivity.class);
                            homeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(homeIntent);
                            finish();
                            dialog.dismiss();
                        }


                );


        dialogHeader_1.show();
    }


    private void resentOtp() {

        showProgressDialog();


        if (CommonUtils.isConnectingToInternet(this)) {
            Call<SuccessResponse> call = networkAPI.resendotp(mobileNO);
            call.enqueue(new Callback<SuccessResponse>() {
                @SuppressLint("Assert")
                @Override
                public void onResponse(@NonNull Call<SuccessResponse> call, @NonNull Response<SuccessResponse> response) {
                    SuccessResponse serverResponse = response.body();

                    dismissProgressDialog();

                    if (response.body() != null) {

                        assert serverResponse != null;
                        if (serverResponse.getResponse().equals("success")) {

                            Toast.makeText(ActivityOtpScreen.this, serverResponse.getMessage(), Toast.LENGTH_SHORT).show();


                        } else {
                            CommonUtils.showerrormsg("Error", serverResponse.getMessage(), ActivityOtpScreen.this);
                        }

                    }
                }

                @Override
                public void onFailure(@NonNull Call<SuccessResponse> call, @NonNull Throwable t) {
                    Log.v("Response", Objects.requireNonNull(t.getMessage()));
                    dismissProgressDialog();
                }
            });
        } else {
            CommonUtils.showerrormsg("Error", "No Internet Connection", this);
        }

    }


}
