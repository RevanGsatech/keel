package com.gsatechwold.keel.ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.google.android.material.navigation.NavigationView;
import com.gsatechwold.keel.R;
import com.gsatechwold.keel.databinding.DashboardBinding;
import com.gsatechwold.keel.ui.base.BaseActivity;
import com.gsatechwold.keel.ui.joblist.AddJobFragment;
import com.gsatechwold.keel.ui.joblist.JobListFragment;
import com.gsatechwold.keel.ui.login.LoginActivity;
import com.gsatechwold.keel.ui.profile.ProfileFragment;

public class DashBoardActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {

    DashboardBinding binding;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.dashboard);

        setToolBar(binding.includeAppBar.toolbar, "Recent Jobs");

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, binding.drawerLayout, binding.includeAppBar.toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        binding.drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        View headerView = binding.navView.getHeaderView(0);
        TextView navUsername = headerView.findViewById(R.id.user_name);
        navUsername.setText("Hi , " + sessionManager.getname());
        binding.navView.setNavigationItemSelectedListener(this);

        loadFragment(new JobListFragment());

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();
        if (id == R.id.nav_home) {
            binding.includeAppBar.toolbar.setTitle("Recent Jobs");
            loadFragment(new JobListFragment());
        } else if (id == R.id.nav_event) {

            binding.includeAppBar.toolbar.setTitle("Event List");

        } else if (id == R.id.nav_emp) {

            binding.includeAppBar.toolbar.setTitle("Add Jobs");
            loadFragment(new AddJobFragment());

        } else if (id == R.id.nav_profile) {

            binding.includeAppBar.toolbar.setTitle("Profile");
            loadFragment(new ProfileFragment());

        } else if (id == R.id.nav_logout) {

            MaterialStyledDialog.Builder dialogHeader = new MaterialStyledDialog.Builder(this)
                    .setIcon(R.drawable.ic_logout)
                    .withDialogAnimation(true)
                    .setTitle("Logout!")
                    .setDescription(R.string.logout_msg)
                    .setHeaderColor(R.color.colorPrimaryDark)
                    .setPositiveText("YES")
                    .setNegativeText("NO")
                    .onPositive((dialog, which) -> {
                        dialog.dismiss();

                        sessionManager.clearUserCredentials();
                        Intent lo = new Intent(getBaseContext(), LoginActivity.class);
                        lo.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(lo);
                        finish();

                    })
                    .onNegative((dialog, which) -> dialog.dismiss());
            dialogHeader.show();


        }
        binding.drawerLayout.closeDrawer(GravityCompat.START);
        return false;
    }

    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.nav_host_fragment, fragment);
        transaction.commit();
    }
}

