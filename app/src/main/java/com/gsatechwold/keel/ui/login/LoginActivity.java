package com.gsatechwold.keel.ui.login;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.gsatechwold.keel.R;
import com.gsatechwold.keel.array.InputLogin;
import com.gsatechwold.keel.databinding.ActivityloginBinding;
import com.gsatechwold.keel.pojo.LoginResponse;
import com.gsatechwold.keel.pojo.SuccessResponse;
import com.gsatechwold.keel.ui.DashBoardActivity;
import com.gsatechwold.keel.ui.base.BaseActivity;
import com.gsatechwold.keel.ui.reg.ActivityOtpScreen;
import com.gsatechwold.keel.ui.reg.RegistrationActivity;
import com.gsatechwold.keel.ui.utils.CommonUtils;

import java.util.Objects;

import de.mrapp.android.dialog.MaterialDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.gsatechwold.keel.ui.utils.CommonUtils.setFocus;

public class LoginActivity extends BaseActivity implements View.OnClickListener, View.OnTouchListener {
    ActivityloginBinding binding;
    LoginViewModel mViewModel;
    InputLogin inputLogin;

    @SuppressLint({"ClickableViewAccessibility"})
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);
        binding = DataBindingUtil.setContentView(this, R.layout.activitylogin);

        // On Click Listener
        binding.tvSignUpClick.setOnClickListener(this);
        binding.btSubmit.setOnClickListener(this);
        binding.tvForgotPass.setOnClickListener(this);

        // On Touch Listener
        binding.etMobile.setOnTouchListener(this);
        binding.etPassword.setOnTouchListener(this);

        mViewModel.getUsers().observe(this, users -> {
            // update UI
        });
    }

    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.tv_sign_upClick) {

            Intent intent = new Intent(this, RegistrationActivity.class);
            startActivity(intent);

        } else if (view.getId() == R.id.bt_submit) {

            if (isFieldsAreValid()) {
                LoginAPi();
            }

        } else if (view.getId() == R.id.tv_forgot_pass) {

            showEditDialog();
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {

        if (view instanceof EditText) {
            view.setFocusableInTouchMode(true);
            ((EditText) view).setCursorVisible(true);
        }
        return false;
    }

    private boolean isFieldsAreValid() {
        String mobile = getString(binding.etMobile);
        String password = getString(binding.etPassword);
        if (CommonUtils.validatePhoneNumber(mobile) != null) {
            setFocus(binding.etMobile, CommonUtils.validatePhoneNumber(mobile));
            return false;
        } else if (CommonUtils.validatePassword(password) != null) {
            setFocus(binding.etPassword, CommonUtils.validatePassword(password));
            return false;
        }

        inputLogin = new InputLogin(mobile, password, "123");
        return true;
    }

    private void LoginAPi() {

        showProgressDialog();

        if (CommonUtils.isConnectingToInternet(this)) {
            Call<LoginResponse> call = networkAPI.login(inputLogin);
            call.enqueue(new Callback<LoginResponse>() {
                @SuppressLint("Assert")
                @Override
                public void onResponse(@NonNull Call<LoginResponse> call, @NonNull Response<LoginResponse> response) {
                    LoginResponse serverResponse = response.body();

                    dismissProgressDialog();

                    if (response.body() != null) {

                        assert serverResponse != null;
                        if (serverResponse.getStatus().equals("success")) {

                            sessionManager.storeUserCredentials(String.valueOf(serverResponse.getUserDetails().getId()), serverResponse.getUserDetails().getMobileNumber(), serverResponse.getUserDetails().getName(), serverResponse.getUserDetails().getAddress(), serverResponse.getUserDetails().getEmail());

                            Intent intent = new Intent(LoginActivity.this, DashBoardActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();

                        } else {
                            CommonUtils.showerrormsg("Error", serverResponse.getMessage(), LoginActivity.this);
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<LoginResponse> call, @NonNull Throwable t) {
                    Log.v("Response", Objects.requireNonNull(t.getMessage()));
                    dismissProgressDialog();
                }
            });
        } else {
            CommonUtils.showerrormsg("Error", "No Internet Connection", this);
        }

    }


    /**
     * This Dailog for Forgot Password to check mobile is registered ot not
     */
    public void showEditDialog() {
        MaterialDialog.Builder dialogBuilder1 = new MaterialDialog.Builder(this);
        dialogBuilder1.setView(R.layout.editviewdailog);
        MaterialDialog dialog1 = dialogBuilder1.create();
        dialog1.show();
        EditText textView = dialog1.findViewById(R.id.et_text);
        TextView saveText = dialog1.findViewById(R.id.saveText);
        TextView cancel = dialog1.findViewById(R.id.cancel);
        cancel.setOnClickListener(view -> dialog1.dismiss());
        saveText.setOnClickListener(view -> {

            if (CommonUtils.validatePhoneNumber(textView.getText().toString()) != null) {
                textView.requestFocus();
                textView.setError(CommonUtils.validatePhoneNumber(textView.getText().toString()));

            } else {
                dialog1.dismiss();
                resend_verifyAPI(textView.getText().toString());
            }
        });

    }

    private void resend_verifyAPI(String mobileNO) {

        showProgressDialog();

        if (CommonUtils.isConnectingToInternet(this)) {
            Call<SuccessResponse> call = networkAPI.resendotp(mobileNO);
            call.enqueue(new Callback<SuccessResponse>() {
                @SuppressLint("Assert")
                @Override
                public void onResponse(@NonNull Call<SuccessResponse> call, @NonNull Response<SuccessResponse> response) {
                    SuccessResponse serverResponse = response.body();

                    dismissProgressDialog();

                    if (response.body() != null) {

                        assert serverResponse != null;
                        if (serverResponse.getResponse().equals("success")) {

                            Intent intent = new Intent(LoginActivity.this, ActivityOtpScreen.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.putExtra("from", "forgot");
                            intent.putExtra("phone", mobileNO);
                            startActivity(intent);

                        } else {
                            CommonUtils.showerrormsg("Error", serverResponse.getMessage(), LoginActivity.this);
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<SuccessResponse> call, @NonNull Throwable t) {
                    Log.v("Response", Objects.requireNonNull(t.getMessage()));
                    dismissProgressDialog();
                }
            });
        } else {
            CommonUtils.showerrormsg("Error", "No Internet Connection", this);
        }

    }


}
