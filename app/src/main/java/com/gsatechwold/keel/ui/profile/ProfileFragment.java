package com.gsatechwold.keel.ui.profile;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.gsatechwold.keel.R;
import com.gsatechwold.keel.databinding.ProfileBinding;
import com.gsatechwold.keel.ui.base.BaseFragment;

public class ProfileFragment extends BaseFragment {

    ProfileBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.profile, container, false);

        binding.name.setText(sessionManager.getname());
        binding.mobileNo.setText(sessionManager.getmobile());
        binding.emailId.setText(sessionManager.getemail());
        binding.address.setText(sessionManager.getaddress());

        return binding.getRoot();
    }
}
