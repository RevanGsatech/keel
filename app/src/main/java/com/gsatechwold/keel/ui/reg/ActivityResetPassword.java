package com.gsatechwold.keel.ui.reg;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;

import com.gsatechwold.keel.R;
import com.gsatechwold.keel.array.InputResetPass;
import com.gsatechwold.keel.databinding.ResetpassBinding;
import com.gsatechwold.keel.pojo.SuccessResponse;
import com.gsatechwold.keel.ui.base.BaseActivity;
import com.gsatechwold.keel.ui.login.LoginActivity;
import com.gsatechwold.keel.ui.utils.CommonUtils;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


@SuppressLint("CheckResult")
public class ActivityResetPassword extends BaseActivity implements View.OnClickListener {

    ResetpassBinding binding;
    String mobileNumber, otp;
    InputResetPass inputVerifyOtp;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.resetpass);

        if (getIntent() != null) {
            mobileNumber = getIntent().getStringExtra("mobile");
            otp = getIntent().getStringExtra("otp");
        }

        binding.btSubmit.setOnClickListener(this);
        binding.etPass.setOnClickListener(this);
        binding.etConfirmPass.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

        if ((view instanceof EditText)) {
            view.setFocusableInTouchMode(true);
            EditText d = (EditText) view;
            d.setCursorVisible(true);
            view.requestFocus();
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            assert imm != null;
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);

        } else if (view.getId() == R.id.bt_submit) {

            if (validateFields()) {

                resetpass();
            }
        }
    }

    private boolean validateFields() {

        String pass = Objects.requireNonNull(binding.etPass.getText()).toString();
        String cpass = Objects.requireNonNull(binding.etConfirmPass.getText()).toString();

        if (CommonUtils.validatePassword(pass) != null) {
            binding.etPass.requestFocus();
            binding.etPass.setError(CommonUtils.validatePassword(pass));
            return false;
        } else if (!pass.equals(cpass)) {
            binding.etConfirmPass.requestFocus();
            binding.etConfirmPass.setError("Confirm Pass Not Matched");
            return false;
        }

        inputVerifyOtp = new InputResetPass(mobileNumber, otp, pass);


        return true;
    }


    private void resetpass() {

        showProgressDialog();

        if (CommonUtils.isConnectingToInternet(this)) {
            Call<SuccessResponse> call = networkAPI.resetpass(inputVerifyOtp);
            call.enqueue(new Callback<SuccessResponse>() {
                @SuppressLint("Assert")
                @Override
                public void onResponse(@NonNull Call<SuccessResponse> call, @NonNull Response<SuccessResponse> response) {
                    SuccessResponse serverResponse = response.body();

                    dismissProgressDialog();

                    if (response.body() != null) {

                        assert serverResponse != null;

                        if (serverResponse.getResponse().equals("success")) {

                            Toast.makeText(ActivityResetPassword.this, serverResponse.getMessage(), Toast.LENGTH_SHORT).show();

                            Intent homeIntent = new Intent(ActivityResetPassword.this, LoginActivity.class);
                            homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(homeIntent);
                            finish();

                        } else {
                            onBackPressed();
                            Toast.makeText(ActivityResetPassword.this, serverResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                }

                @Override
                public void onFailure(@NonNull Call<SuccessResponse> call, @NonNull Throwable t) {
                    Log.v("Response", Objects.requireNonNull(t.getMessage()));
                    dismissProgressDialog();
                }
            });
        } else {
            CommonUtils.showerrormsg("Error", "No Internet Connection", this);
        }

    }


}
