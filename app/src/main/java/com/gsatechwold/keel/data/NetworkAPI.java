package com.gsatechwold.keel.data;


import com.gsatechwold.keel.array.InputAddJob;
import com.gsatechwold.keel.array.InputApplyJob;
import com.gsatechwold.keel.array.InputLogin;
import com.gsatechwold.keel.array.InputReg;
import com.gsatechwold.keel.array.InputResetPass;
import com.gsatechwold.keel.array.InputVerifyOtp;
import com.gsatechwold.keel.pojo.JobListRespone;
import com.gsatechwold.keel.pojo.LoginResponse;
import com.gsatechwold.keel.pojo.SuccessResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface NetworkAPI {

    //Registration
    @POST("registration")
    Call<SuccessResponse> regisration(@Body InputReg inputReg);

    //login
    @POST("login")
    Call<LoginResponse> login(@Body InputLogin inputLogin);

    //Verify Otp
    @POST("verifyotp")
    Call<SuccessResponse> verifyotp(@Body InputVerifyOtp inputVerifyOtp);

    //Re-send OTP
    @GET("sendotp")
    Call<SuccessResponse> resendotp(@Query("mobile_number") String mobileno);

    //Reset pass
    @POST("resetPassword")
    Call<SuccessResponse> resetpass(@Body InputResetPass inputResetPass);

    //Job Time line
    @GET("timeline")
    Call<JobListRespone> timeline(@Query("user_id") String user_id, @Query("lattitude") String lattitude, @Query("longitude") String longitude);

    //Add Employer
    @POST("addEmployerForm")
    Call<SuccessResponse> addEmployerForm(@Body InputAddJob addJob);

    //Apply job
    @POST("applyJob")
    Call<SuccessResponse> applyJob(@Body InputApplyJob inputApplyJob);


}
