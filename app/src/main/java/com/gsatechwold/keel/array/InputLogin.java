package com.gsatechwold.keel.array;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InputLogin {

    @SerializedName("mobile_number")
    @Expose
    public String mobile_number;

    @SerializedName("password")
    @Expose
    public String password;

    @SerializedName("firebase_id")
    @Expose
    public String firebase_id;

    public InputLogin(String mobile_number, String password, String firebase_id) {
        this.mobile_number = mobile_number;
        this.password = password;
        this.firebase_id = firebase_id;
    }
}
