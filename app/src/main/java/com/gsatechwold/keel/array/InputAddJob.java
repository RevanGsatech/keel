package com.gsatechwold.keel.array;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InputAddJob {

    @SerializedName("user_id")
    @Expose
    public String userId;

    @SerializedName("image")
    @Expose
    public String image;

    @SerializedName("job_title")
    @Expose
    public String jobTitle;

    @SerializedName("job_description")
    @Expose
    public String jobDescription;

    @SerializedName("job_address")
    @Expose
    public String jobAddress;

    @SerializedName("village")
    @Expose
    public String village;

    @SerializedName("lattitude")
    @Expose
    public String lattitude;

    @SerializedName("longitude")
    @Expose
    public String longitude;
    @SerializedName("pricing")
    @Expose
    public String pricing;

    public InputAddJob(String userId, String image, String jobTitle, String jobDescription, String jobAddress, String village, String lattitude, String longitude, String pricing, String noOfDays, String noOfEmployeeRequired, String contact) {
        this.userId = userId;
        this.image = image;
        this.jobTitle = jobTitle;
        this.jobDescription = jobDescription;
        this.jobAddress = jobAddress;
        this.village = village;
        this.lattitude = lattitude;
        this.longitude = longitude;
        this.pricing = pricing;
        this.noOfDays = noOfDays;
        this.noOfEmployeeRequired = noOfEmployeeRequired;
        this.contact = contact;
    }

    @SerializedName("no_of_days")
    @Expose
    public String noOfDays;
    @SerializedName("no_of_employee_required")
    @Expose
    public String noOfEmployeeRequired;
    @SerializedName("contact")
    @Expose
    public String contact;
}
