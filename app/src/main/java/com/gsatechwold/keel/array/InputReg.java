package com.gsatechwold.keel.array;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InputReg {

    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("mobile_number")
    @Expose
    public String mobileNumber;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("village")
    @Expose
    public String village;
    @SerializedName("address")
    @Expose
    public String address;
    @SerializedName("education")
    @Expose
    public String education;
    @SerializedName("password")
    @Expose
    public String password;

    public InputReg(String name, String mobileNumber, String email, String village, String address, String education, String password) {
        this.name = name;
        this.mobileNumber = mobileNumber;
        this.email = email;
        this.village = village;
        this.address = address;
        this.education = education;
        this.password = password;
    }
}
