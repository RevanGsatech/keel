package com.gsatechwold.keel;

import android.content.Context;
import android.content.SharedPreferences;

public class SessionManager {


    // Shared preferences file name
    private static final String PREF_NAME = "snow-intro-slider";
    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";

    private static final String IS_LOGIN = "isLogin";
    private static final String USER_ID = "userID";
    private static final String MOBILE_NUMBER = "mobileNumber";
    private static final String USER_NAME = "username";
    private static final String Address = "address";
    private static final String EmailId = "email";


    // shared pref mode
    int PRIVATE_MODE = 0;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;


    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }


    public void userLoggedIn() {
        editor.putBoolean(IS_LOGIN, true);
        editor.commit();
    }


    public boolean isUserLoggedIn() {
        return pref.getBoolean(IS_LOGIN, false);
    }


    public void storeUserCredentials(String userID, String mobileNumber, String username, String address, String email) {
        editor.putString(USER_ID, userID);
        editor.putString(MOBILE_NUMBER, mobileNumber);
        editor.putString(USER_NAME, username);
        editor.putString(Address, address);
        editor.putString(EmailId, email);
        editor.commit();
        userLoggedIn();
    }

    public String getUserID() {
        return pref.getString(USER_ID, "");
    }


    public String getname() {
        return pref.getString(USER_NAME, "");
    }

    public String getmobile() {
        return pref.getString(MOBILE_NUMBER, "");
    }

    public String getemail() {
        return pref.getString(EmailId, "");
    }

    public String getaddress() {
        return pref.getString(Address, "");
    }

    public boolean isFirstTimeLaunch() {
        return pref.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }

    public void setFirstTimeLaunch(boolean isFirstTime) {
        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        editor.commit();
    }


    public void clearUserCredentials() {
        editor.clear();
        editor.commit();
    }

}
